"""libretacontactos URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from contactos import views
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from contactos.views import ContactoListado, ContactoDetalle, ContactoCrear, ContactoActualizar, ContactoEliminar

urlpatterns = [
    path('admin/', admin.site.urls),
    path('contactos/',views.Contactos),
    # path('admin/', admin.site.urls),
 
    # La ruta 'leer' en donde listamos todos los registros o postres de la Base de Datos
    path('contactos/', ContactoListado.as_view(template_name = "contactos.html"), name='leer'),
 
    # La ruta 'detalles' en donde mostraremos una página con los detalles de un postre o registro 
    path('detalle/<int:pk>', ContactoDetalle.as_view(template_name = "detalles.html"), name='detalles'),
 
    # La ruta 'crear' en donde mostraremos un formulario para crear un nuevo postre o registro  
    path('Contacto/crear', ContactoCrear.as_view(template_name = "Contacto/crear.html"), name='crear'),
 
    # La ruta 'actualizar' en donde mostraremos un formulario para actualizar un postre o registro de la Base de Datos 
    path('editar/<int:pk>', ContactoActualizar.as_view(template_name = "editar.html"), name='editar'), 
 
    # La ruta 'eliminar' que usaremos para eliminar un postre o registro de la Base de Datos 
    path('eliminar/<int:pk>', ContactoEliminar.as_view(), name='eliminar'),    

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
