from django.contrib import admin
from django.contrib import admin

# Register your models here.
from contactos.models import Contacto

class ContactosAdmin(admin.ModelAdmin):
    list_display=("nombre","apellido","celular","fotografia","correo")
    search_fields=("nombre","apellido")    
    list_filter=("nombre",)

admin.site.register(Contacto,ContactosAdmin)
# Register your models here.
