from django.shortcuts import render
from django.views.generic import ListView, DetailView 
from django.views.generic.edit import CreateView, UpdateView, DeleteView
# Nos sirve para redireccionar despues de una acción revertiendo patrones de expresiones regulares 
from django.urls import reverse
 
# Habilitamos el uso de mensajes en Django
from django.contrib import messages 
 
# Habilitamos los mensajes para class-based views 
from django.contrib.messages.views import SuccessMessageMixin 
 
# Habilitamos los formularios en Django
from django import forms
from contactos.models import Contacto
from contactos.forms import ContactosForm
from django.http import HttpResponse
from django.template import loader
from django.http import HttpResponseRedirect

class ContactoListado(ListView): 
    model = Contacto # Llamamos a la clase 'Postres' que se encuentra en nuestro archivo 'models.py'

class ContactoCrear(SuccessMessageMixin, CreateView): 
    model = Contacto # Llamamos a la clase 'Postres' que se encuentra en nuestro archivo 'models.py'
    form = Contacto # Definimos nuestro formulario con el nombre de la clase o modelo 'Postres'
    fields = "__all__" # Le decimos a Django que muestre todos los campos de la tabla 'postres' de nuestra Base de Datos 
    success_message = 'Contacto Creado Correctamente !' # Mostramos este Mensaje luego de Crear un Postre
 
    # Redireccionamos a la página principal luego de crear un registro o postre
    def get_success_url(self):        
        return reverse('leer') # Redireccionamos a la vista principal 'leer'

class ContactoDetalle(DetailView): 
    model = Contacto # Llamamos a la clase 'Postres' que se encuentra en nuestro archivo 'models.py'

class ContactoActualizar(SuccessMessageMixin, UpdateView): 
    model = Contacto # Llamamos a la clase 'Postres' que se encuentra en nuestro archivo 'models.py' 
    form = Contacto # Definimos nuestro formulario con el nombre de la clase o modelo 'Postres' 
    fields = "__all__" # Le decimos a Django que muestre todos los campos de la tabla 'postres' de nuestra Base de Datos 
    success_message = 'Contracto Actualizado Correctamente !' # Mostramos este Mensaje luego de Editar un Postre 
 
    # Redireccionamos a la página principal luego de actualizar un registro o postre
    def get_success_url(self):               
        return reverse('leer') # Redireccionamos a la vista principal 'leer'

class ContactoEliminar(SuccessMessageMixin, DeleteView): 
    model = Contacto 
    form = Contacto
    fields = "__all__"     
 
    # Redireccionamos a la página principal luego de eliminar un registro o postre
    def get_success_url(self): 
        success_message = 'Contacto Eliminado Correctamente !' # Mostramos este Mensaje luego de Editar un Postre 
        messages.success (self.request, (success_message))       
        return reverse('leer') # Redireccionamos a la vista principal 'leer'

def Contactos(request):
    template = loader.get_template('contactos.html')
    if request.method == 'POST':

        form = ContactosForm(request.POST)
        print(form)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/contactos/')
        else:
            return HttpResponseRedirect('/Error/')
    else:   
            form = ContactosForm()
            context ={'form': form,
                    'contactos':Contacto.objects.all(),   
            }
    return HttpResponse(template.render(context, request))