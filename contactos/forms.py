from django import forms
from .models import Contacto


class ContactosForm(forms.ModelForm):
    class Meta:
        model = Contacto
        fields = [
        'id',
        'nombre',
        'apellido',
        'celular',
        'fotografia',
        'correo'
        ]
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'apellido' : forms.TextInput(attrs={'class':'form-control'}),
            'celular' : forms.TextInput(attrs={'class':'form-control', 'type':'number'}),
            # 'fotografia' : forms.ImageField(),
            'correo' : forms.TextInput(attrs={'class':'form-control', 'type':'email'})   
        }
